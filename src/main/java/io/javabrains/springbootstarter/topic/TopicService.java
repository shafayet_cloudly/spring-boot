package io.javabrains.springbootstarter.topic;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService {
    private List<Topic> getAllTopics = new ArrayList<>(Arrays.asList(
            new Topic("spring", "Spring Framework", "Spring Framework Description"),
            new Topic("Java", "Core Java", "Java Core Description"),
            new Topic("spring", "Spring Framework", "Spring Framework Description"),
            new Topic("Java1", "Core Java 1", "Java Core Description 1"),
            new Topic("JavaScript", "Core JavaScript", "JavaScript Description")
    ));

    public List<Topic> getGetAllTopics(){
        return getAllTopics;
    };

    public Topic getTopic(String id) {
        return getAllTopics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
    };

    public void addTopic(Topic topic) {
        getAllTopics.add(topic);
    }

    public void updateTopic(Topic topic, String id) {
        for (int i=0; i< getAllTopics.size(); i++) {
            Topic t = getAllTopics.get(i);
            if(t.getId().equals(id)) {
                getAllTopics.set(i, topic);
                return;
            }
        }
    }

    public void deleteTopic(String id)
    {
        getAllTopics.removeIf(t -> t.getId().equals(id));
    }
}
