package io.javabrains.springbootstarter.data;

public class Data {
    public String id;
    public String name;
    public String description;

    public Data() {

    }

    public Data(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
