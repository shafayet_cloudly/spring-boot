package io.javabrains.springbootstarter.data;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DataService {
    private List<Data> allData =  Arrays.asList(
        new Data("902893823098", "Shakib", "He is a good boy"),
            new Data("433453454355", "Sazzad", "He is a good boy"),
            new Data("435345466645", "Supto", "He is a good boy")
    );

    public List<Data> getDatas(){
        return allData;
    }

    public Data getData(String id) {
        return allData.stream().filter(t -> t.getName().equals(id)).findFirst().get();
    }
}
