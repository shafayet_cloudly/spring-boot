package io.javabrains.springbootstarter.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class DataController {

    @Autowired
    private DataService DataService;

    @RequestMapping("/data")
    public List<Data> getAllData(){
        return DataService.getDatas();
    }

    @RequestMapping("/data/{dataId}")
    public Data getDetailsOfData(@PathVariable("dataId") String id) {

        return DataService.getData(id);
    }
}
